#Project description 

Spring MVC application for solar irradiation calculation on a panel. Contains one REST service request. 
Front-end in AngularJS.   
  
User needs to enter tilt, azimuth and geographical location to web user interface.
Then solar irradiance on the panel is calculated for every hour.  
  
Base data ise requested through PlanetOS API.

##Installing dependencies 
1) Install maven dependencies: mvn install  
2) Install bower dependencies: bower install  

##Running the project
1) Run project from Java main  
2) Access on http://localhost:8080  

##Deploying
1) Run "mvn package" from project root
2) Deploy .war file from target 
