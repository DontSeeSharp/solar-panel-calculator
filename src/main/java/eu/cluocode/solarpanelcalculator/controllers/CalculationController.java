package eu.cluocode.solarpanelcalculator.controllers;


import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Hendrig on 10.03.2017.
 */
@RestController
public class CalculationController {

    @RequestMapping(value = "/getCalculations", method = RequestMethod.GET, produces = "application/json")
    public String getCalculations(@RequestParam Map<String, String> requestParams) {

        String lat = requestParams.get("lat");
        String lng = requestParams.get("lng");
        int tilt = Integer.parseInt(requestParams.get("tilt"));
        int azimuth = Integer.parseInt(requestParams.get("azimuth"));

        return parseJsonAndCalculate(getDataFromPlanetOS(lat, lng), tilt, azimuth);
    }

    private static String getDataFromPlanetOS(String lat, String lng) {
        String currentDateTime = getCurrentDateTimeForPlanetOS();
        System.out.println(currentDateTime);

        final String uri = "http://api.planetos.com/v1/datasets/noaa_gfs_pgrb2_global_forecast_recompute_0.25degree/point?apikey=a7017583aeb944d2b8bfec81ff9a2363&lon=" + lng + "&lat=" + lat + "&verbose=false&count=120&variable=dswrfsfc_1_Hour_Average&start=" + currentDateTime;

        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.getForObject(uri, String.class);
    }

    /**
     * Date for planetOS specifications.
     * @return string of date in format yyyy-MM-ddTHH:mm:00Z
     */
    private static String getCurrentDateTimeForPlanetOS() {
        String currentYearMonthDay = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String currentHourAndMinutes = new SimpleDateFormat("HH:mm").format(new Date());
        return currentYearMonthDay + "T" + currentHourAndMinutes + ":00Z";
    }

    /**
     * Parse planetOS json and return array containing time and corresponding solar irradiance.
     * @param jsonData Data acquired from planetOS api. It contains duplicates so they have to be checked.
     * @param tilt tilt of the panel, needed for calculations
     * @param azimuth azimuth of the panel, needed for calculcations
     * @return Array in form [{"time":"yyyy-mm-ddTHH:mm:ss","value":0}]
     */
    private String parseJsonAndCalculate(String jsonData, int tilt, int azimuth) {
        LinkedHashMap<String, String> jsonOrderedMap = new LinkedHashMap<String, String>();
        JSONObject obj = new JSONObject(jsonData);

        JSONArray arr = obj.getJSONArray("entries");

        JSONArray returnArray = new JSONArray();

        HashMap<String, String> hashMap = new HashMap<>();

        for (int i=0; i< arr.length(); i++) {
            JSONObject tempObj = new JSONObject();
            String time = arr.getJSONObject(i).getJSONObject("axes").getString("time");
            if (!hashMap.containsKey(time)) {
                hashMap.put(time, "dummy");
                int initialRadiation =  arr.getJSONObject(i).getJSONObject("data").getInt("dswrfsfc_1_Hour_Average");
                int calculatedRadiation = calculateRatiation(time, initialRadiation, tilt, azimuth);
                tempObj.put("time", time);
                tempObj.put("value", calculatedRadiation);
            }
            returnArray.put(tempObj);
        }
        System.out.println(returnArray.toString());
        return returnArray.toString();
    }

    /**
     * Calculates radiation to a solar panel which is located at given point, time and angle relative to sun.
     * @param time - time in format yyyy-mm-ddThh:mm:ss:ms
     * @param initialRadiation - radiation to a horizontal surface W / m^2
     * @param tilt - tilt from ground position in degrees 0 - horizontal and 90 - vertical
     * @param azimuth - asimuth from north in degrees 0 is north , 180 south and 360 north again
     * @return - direct solar radiation that is calculated from the given parameters.
     */
    private int calculateRatiation(String time, int initialRadiation, int tilt, int azimuth) {
        //TODO - do some calculations to get the correct value
        //Since you emailed, that you don't care about the physical calculations that much, I didn't start doing them.
        //Do some calculations.
        return initialRadiation;
    }

}
