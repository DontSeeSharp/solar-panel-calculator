/**
 * Controller displaying data on web page.
 */

controllers.controller('resultsController', ['$routeParams', '$scope', '$location', function ($routeParams, $scope, $location) {
    var lat = $routeParams.lat;
    var lng = $routeParams.lng;
    var tilt = $routeParams.tilt;
    var azimuth = $routeParams.azimuth;
    if (lat == null || lng == null || tilt == null || azimuth == null) {
        $location.path("/");
    }
    $scope.loadingHidden = true;

    var chartDataArray = [];
    chartDataArray.push(['Time', "W per square meter"]);

    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        jQuery.ajax({
            url: (window.location.href.split("#!/")[0] + "/getCalculations?lat=" + lat
            + "&lng=" + lng + "&tilt=" + tilt + "&azimuth=" + azimuth)
    }).
        then(function (data) {
            for (var i in data) {
                if (data.hasOwnProperty(i)) {
                    chartDataArray.push([new Date(data[i].time), data[i].value])
                }
            }

            var data = google.visualization.arrayToDataTable(chartDataArray);

            var options = {
                title: 'Solar radiation on panel',
                hAxis: {
                    title: 'Year',
                    titleTextStyle: {
                        color: '#333'
                    },
                    slantedText: true,
                    slantedTextAngle: 80
                },
                vAxis: {
                    minValue: 0
                },
                explorer: {
                    axis: 'horizontal',
                    keepInBounds: true,
                    maxZoomIn: 4.0
                },
                colors: ['#D44E41'],
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            $scope.$apply(function () {
                $scope.loadingHidden = false;
            });

            chart.draw(data, options);
        });

    }
}]);