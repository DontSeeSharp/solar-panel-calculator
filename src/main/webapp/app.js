'use strict';

/* App Module */

var app = angular.module('app', ['ngRoute','controllers']);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'data_enter/data-enter.html',
            controller: 'dataEnterController'
        }).when('/results', {
            templateUrl: 'results/results.html',
            controller: 'resultsController'
        }).
        otherwise({
            redirectTo: '/'
        });
    }
]);


