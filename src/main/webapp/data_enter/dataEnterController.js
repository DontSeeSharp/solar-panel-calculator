/**
 * Controller for data entering.
 */

controllers.controller('dataEnterController', ['NgMap', '$scope', '$location', function (NgMap, $scope, $location) {
    console.log("Entered data entering controller.");
    $scope.panel = {};
    $scope.panel.tilt = 0;
    $scope.panel.azimuth = 0;

    $scope.changeLocation = function () {
        $location.path('/results/').search({
            tilt: $scope.panel.tilt,
            azimuth: $scope.panel.azimuth,
            lat: $scope.currentCenterLocation.lat,
            lng: $scope.currentCenterLocation.lng
        });
    };

    //Code for google maps api
    var vm = this;
    $scope.types = "['address']";
    $scope.placeChanged = function () {
        console.log("triggered");
        vm.place = this.getPlace();
        vm.map.setCenter(vm.place.geometry.location);
    };
    NgMap.getMap().then(function (map) {
        vm.map = map;
    });

    $scope.location = {};

    $scope.setInitialCoordinates = function () {
        $scope.location.lat = 59.395896;
        $scope.location.lng = 24.671332;
        $scope.location.zoom = 18;
    };
    $scope.setInitialCoordinates();

    $scope.currentCenterLocation = {"lat": $scope.location.lat, "lng": $scope.location.lng};

    $scope.getCenterLocation = function () {
        $scope.currentCenterLocation.lat = vm.map.getCenter().lat();
        $scope.currentCenterLocation.lng = vm.map.getCenter().lng();
        console.log($scope.currentCenterLocation);
    };
    /*touch directives for google maps mobile versions */
}]).directive('myTouchend', [function () {
    return function (scope, element, attr) {
        element.on('touchend', function (event) {
            scope.$apply(function () {
                scope.$eval(attr.myTouchend);
            });
        });
    };
}]);

